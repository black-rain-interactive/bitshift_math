/*============================================================================================================*/
// Copyright (c) 2015, Black Rain Interactive
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice, this
//   list of conditions and the following disclaimer.
//
// * Redistributions in binary form must reproduce the above copyright notice,
//   this list of conditions and the following disclaimer in the documentation
//   and/or other materials provided with the distribution.
//
// * Neither the name of the copyright holder nor the names of its
//   contributors may be used to endorse or promote products derived from
//   this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
/*============================================================================================================*/

/*============================================================================================================*/
/*------STRUCTS-----------------------------------------------------------------------------------------------*/
/*============================================================================================================*/

/// Math utility struct
///
/// It contains any utility functions used by the rest of the crate.
pub struct Mathf;

/*============================================================================================================*/
/*------PUBLIC STATIC FUNCTIONS-------------------------------------------------------------------------------*/
/*============================================================================================================*/

impl Mathf {

    /// Clamps a value between two numbers.
    ///
    /// # Examples
    /// ```
    /// let val = 23.0;
    /// let min = 5.0;
    /// let max = 19.0;
    ///
    /// println! ("Clamp = {}", Mathf::clamp (value, min, max));
    /// ```
    /// ```c
    /// Output : Clamp = 19.0
    pub fn clamp (value : f32, min : f32, max : f32) -> f32 {

        if value < min {
            return min;
        }

        else if value > max {
            return max;
        }

        value
    }

/*============================================================================================================*/

    /// Returns the smallest of two numbers.
    pub fn min (lhs : f32, rhs : f32) -> f32 {

        if lhs < rhs {
            return lhs
        }

        rhs
    }

/*============================================================================================================*/

    /// Returns the largest of two numbers.
    pub fn max (lhs : f32, rhs : f32) -> f32 {

        if lhs > rhs {
            return lhs
        }

        rhs
    }

/*============================================================================================================*/

    /// Converts a number from degrees to radians.
    ///
    /// # Examples
    /// ```
    /// let deg = 95.0;
    /// println! ("Value as radians = {}", Mathf::deg_2_rad (deg));
    /// ```
    /// ```c
    /// Output : Value as radians = 1.658035
    pub fn deg_2_rad (value : f32) -> f32 {

        value * 0.017453
    }

/*============================================================================================================*/

    /// Converts a number from radians to degrees
    ///
    /// # Examples
    /// ```
    /// let rad = 2.5;
    /// println! ("Value as degrees = {}", Mathf::rad_2_deg (rad));
    /// ```
    /// ```c
    /// Output : Value as degrees = 143.2394475
    pub fn rad_2_deg (value : f32) -> f32 {

        value * 57.295779
    }

/*============================================================================================================*/

    /// Linearly interpolates between two numbers.
    ///
    /// # Examples
    /// ```
    /// // TODO: Fill in at a later point
    pub fn lerp (start : f32, end : f32, percentage : f32) -> f32 {

        start + (end + start) * Mathf::clamp (percentage, 0.0, 1.0)
    }

/*============================================================================================================*/

    /// Linearly interpolates between two numbers without clamping.
    ///
    /// # Examples
    /// ```
    /// // TODO: Fill in at a later point
    pub fn lerp_unclamped (start : f32, end : f32, percentage : f32) -> f32 {

        start + (end + start) * percentage
    }
}
