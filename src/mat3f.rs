/*============================================================================================================*/
// Copyright (c) 2015, Black Rain Interactive
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice, this
//   list of conditions and the following disclaimer.
//
// * Redistributions in binary form must reproduce the above copyright notice,
//   this list of conditions and the following disclaimer in the documentation
//   and/or other materials provided with the distribution.
//
// * Neither the name of the copyright holder nor the names of its
//   contributors may be used to endorse or promote products derived from
//   this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
/*============================================================================================================*/

use {Vec2f, Vec3f, Mathf};

use std::ops::*;
use std::cmp::PartialEq;

/*============================================================================================================*/
/*------STRUCTS-----------------------------------------------------------------------------------------------*/
/*============================================================================================================*/

/// 3x3 matrix.
///
/// This struct represents a 3x3 matrix. It is normally used for 2D graphics transformations,
/// such as translation, rotation, and scaling.
#[derive (Copy, Clone)]
pub struct Mat3f {

    // Private
    _value : [Vec3f; 3]
}

/*============================================================================================================*/
/*------PUBLIC FUNCTIONS--------------------------------------------------------------------------------------*/
/*============================================================================================================*/

impl Mat3f {

    /// Formats the matrix as a string.
    ///
    /// # Examples
    /// ```
    /// let mat = Mat3f::identity ();
    /// println! ("{}", mat.to_string ());
    /// ```
    /// ```c
    /// Output : [1, 0, 0]
    ///          [0, 1, 0]
    ///          [0, 0, 1]
    pub fn to_string (&self) -> String {

        format! ("[{}]\n[{}]\n[{}]", self [0].to_string (),
                                     self [1].to_string (),
                                     self [2].to_string ())
    }

/*============================================================================================================*/
/*------PUBLIC STATIC FUNCTIONS-------------------------------------------------------------------------------*/
/*============================================================================================================*/

    /// Creates a matrix with default values.
    ///
    /// # Examples
    /// ```
    /// let mat = Mat3f::new (); // Returns a matrix with all zeros
    pub fn new () -> Mat3f {

        Mat3f {_value : [Vec3f::new (); 3]}
    }

/*============================================================================================================*/

    /// Returns a new identity matrix
    pub fn identity () -> Mat3f {

        Mat3f {_value : [Vec3f {x : 1.0, y : 0.0, z : 0.0},
                         Vec3f {x : 0.0, y : 1.0, z : 0.0},
                         Vec3f {x : 0.0, y : 0.0, z : 1.0}]}
    }

/*============================================================================================================*/

    /// Returns the determinant of a matrix.
    /// This is used as part of the inverse matrix calculation.
    ///
    /// # Examples
    /// ```
    /// let mat = Mat3f::identity ();
    /// println! ("Determinant = {}", Mat3f::determinant (&mat));
    /// ```
    /// ```c
    /// Output : Determinant = 1.0
    pub fn determinant (m : &Mat3f) -> f32 {

        m [0][0] * m [1][1] * m [2][2] +
        m [0][1] * m [1][2] * m [2][0] +
        m [0][2] * m [1][0] * m [2][1] -
        m [0][0] * m [1][2] * m [2][1] -
        m [0][1] * m [1][0] * m [2][2] -
        m [0][2] * m [1][1] * m [2][0]
    }

/*============================================================================================================*/

    /// Returns the inverse of a matrix.
    ///
    /// When multiplied by the original matrix, the end result will be an identity matrix.
    pub fn inverse (m : &Mat3f) -> Mat3f {

        let det = 1.0 / Mat3f::determinant (m);

        Mat3f {_value : [Vec3f {x : (m [1][1] * m [2][2] - m [1][2] * m [2][1]) * det,
                                y : (m [0][2] * m [2][1] - m [0][1] * m [2][2]) * det,
                                z : (m [0][1] * m [1][2] - m [0][2] * m [1][1]) * det},

                         Vec3f {x : (m [1][2] * m [2][0] - m [1][0] * m [2][2]) * det,
                                y : (m [0][0] * m [2][2] - m [0][2] * m [2][0]) * det,
                                z : (m [0][2] * m [1][0] - m [0][0] * m [1][2]) * det},

                         Vec3f {x : (m [1][0] * m [2][1] - m [1][1] * m [2][0]) * det,
                                y : (m [0][1] * m [2][0] - m [0][0] * m [2][1]) * det,
                                z : (m [0][0] * m [1][1] - m [0][1] * m [1][0]) * det}]}
    }

/*============================================================================================================*/

    /// Returns a 2D translation matrix.
    ///
    /// It represents a transformation in 2D space.
    /// When combined with a rotation and scale matrix, it creates a model matrix.
    ///
    /// # Examples
    /// ```
    /// let pos = Vec2f {x : 10.0, y : 43.0};
    /// let mat = Mat3f::translate (&pos);
    pub fn translate (position : &Vec2f) -> Mat3f {

        Mat3f {_value : [Vec3f {x : 1.0, y : 0.0, z : position.x},
                         Vec3f {x : 0.0, y : 1.0, z : position.y},
                         Vec3f {x : 0.0, y : 0.0, z : 1.0}]}
    }

/*============================================================================================================*/

    /// Returns a 2D rotation matrix.
    ///
    /// It represents a rotation in 2D space.
    /// When combined with a translation and scale matrix, it creates a model matrix.
    ///
    /// It takes in a single f32 argument, which is the rotation in degrees.
    ///
    /// # Examples
    /// ```
    /// let rot = 45.0;
    /// let mat = Mat3f::rotate (rot);
    pub fn rotate (rotation : f32) -> Mat3f {

        // Convert rotation to radians
        let rad_rotation = Mathf::deg_2_rad (rotation);

        let z_sin = rad_rotation.sin ();
        let z_cos = rad_rotation.cos ();

        // Transform z-axis
        Mat3f {_value : [Vec3f {x : z_cos, y : -z_sin, z : 0.0},
                         Vec3f {x : z_sin, y : z_cos,  z : 0.0},
                         Vec3f {x : 0.0,   y : 0.0,    z : 1.0}]}
    }

/*============================================================================================================*/

    /// Returns a 2D scale matrix.
    ///
    /// It represents a scale in 2D space.
    /// When combined with a rotation and rotation matrix, it creates a model matrix.
    ///
    /// # Examples
    /// ```
    /// let scale = Vec2f {x : 5.0, y : 1.0};
    /// let mat = Mat3f::scale (&pos);
    pub fn scale (scale : &Vec2f) -> Mat3f {

        Mat3f {_value : [Vec3f {x : scale.x, y : 0.0,     z : 0.0},
                         Vec3f {x : 0.0,     y : scale.y, z : 0.0},
                         Vec3f {x : 0.0,     y : 0.0,     z : 1.0}]}
    }
}

/*============================================================================================================*/
/*------OPERATOR OVERLOADS------------------------------------------------------------------------------------*/
/*============================================================================================================*/

impl Add for Mat3f {

    type Output = Mat3f;

    // Addition operator (matrix)
    fn add (self, rhs : Mat3f) -> Mat3f {

        Mat3f {_value : [self [0] + rhs [0],
                         self [1] + rhs [1],
                         self [2] + rhs [2]]}
    }
}

/*============================================================================================================*/

impl Add <Vec3f> for Mat3f {

    type Output = Mat3f;

    // Addition operator (vector)
    fn add (self, rhs : Vec3f) -> Mat3f {

        Mat3f {_value : [self [0] + rhs,
                         self [1] + rhs,
                         self [2] + rhs]}
    }
}

/*============================================================================================================*/

impl AddAssign for Mat3f {

    // Addition assignment operator (matrix)
    fn add_assign (&mut self, rhs : Mat3f) {

            self._value = (*self + rhs)._value;
    }
}

/*============================================================================================================*/

impl AddAssign <Vec3f> for Mat3f {

    // Addition assignment operator (vector)
    fn add_assign (&mut self, rhs : Vec3f) {

            self._value = (*self + rhs)._value;
    }
}

/*============================================================================================================*/

impl Sub for Mat3f {

    type Output = Mat3f;

    // Subtraction operator (matrix)
    fn sub (self, rhs : Mat3f) -> Mat3f {

        Mat3f {_value : [self [0] - rhs [0],
                         self [1] - rhs [1],
                         self [2] - rhs [2]]}
    }
}

/*============================================================================================================*/

impl Sub <Vec3f> for Mat3f {

    type Output = Mat3f;

    // Subtraction operator (vector)
    fn sub (self, rhs : Vec3f) -> Mat3f {

        Mat3f {_value : [self [0] - rhs,
                         self [1] - rhs,
                         self [2] - rhs]}
    }
}

/*============================================================================================================*/

impl SubAssign for Mat3f {

    // Subtraction assignment operator (matrix)
    fn sub_assign (&mut self, rhs : Mat3f) {

        self._value = (*self - rhs)._value;
    }
}

/*============================================================================================================*/

impl SubAssign <Vec3f> for Mat3f {

    // Subtraction assignment operator (vector)
    fn sub_assign (&mut self, rhs : Vec3f) {

            self._value = (*self - rhs)._value;
    }
}

/*============================================================================================================*/

impl Mul for Mat3f {

    type Output = Mat3f;

    // Multiplication operator (matrix)
    fn mul (self, rhs : Mat3f) -> Mat3f {

        let mut return_matrix = Mat3f::new ();

        for row in 0..3 {

            for col in 0..3 {

                for inner in 0..3 {
                    return_matrix [row][col] += self [row][inner] * rhs [inner][col];
                }
            }
        }

        return_matrix
    }
}

/*============================================================================================================*/

impl MulAssign for Mat3f {

    // Multiplication assignment operator (matrix)
    fn mul_assign (&mut self, rhs : Mat3f) {

        self._value = (*self * rhs)._value;
    }
}

/*============================================================================================================*/

impl PartialEq for Mat3f {

    // Equal to operator
    fn eq (&self, rhs : &Mat3f) -> bool {

        self [0] == rhs [0] &&
        self [1] == rhs [1] &&
        self [2] == rhs [2]
    }

/*============================================================================================================*/

    // Not equal to operator
    fn ne (&self, rhs : &Mat3f) -> bool {

        self [0] != rhs [0] ||
        self [1] != rhs [1] ||
        self [2] != rhs [2]
    }
}

/*============================================================================================================*/

impl Index <u8> for Mat3f {

    type Output = Vec3f;

    // Index operator (immutable)
    fn index (&self, index : u8) -> &Vec3f {

        match index {

            0 => return &self._value [0],
            1 => return &self._value [1],
            2 => return &self._value [2],
            _ => unreachable! ("Index out of range for Mat3f")
        }
    }
}

/*============================================================================================================*/

impl IndexMut <u8> for Mat3f {

    // Index operator (mutable)
    fn index_mut (&mut self, index : u8) -> &mut Vec3f {

        match index {

            0 => return &mut self._value [0],
            1 => return &mut self._value [1],
            2 => return &mut self._value [2],
            _ => unreachable! ("Index out of range for Mat3f")
        }
    }
}
