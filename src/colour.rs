/*============================================================================================================*/
// Copyright (c) 2015, Black Rain Interactive
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice, this
//   list of conditions and the following disclaimer.
//
// * Redistributions in binary form must reproduce the above copyright notice,
//   this list of conditions and the following disclaimer in the documentation
//   and/or other materials provided with the distribution.
//
// * Neither the name of the copyright holder nor the names of its
//   contributors may be used to endorse or promote products derived from
//   this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
/*============================================================================================================*/

use Mathf;

use std::ops::*;
use std::cmp::PartialEq;

/*============================================================================================================*/
/*------STRUCTS-----------------------------------------------------------------------------------------------*/
/*============================================================================================================*/

/// RGBA colour representation.
///
/// This struct represents colour in BitShift, and is used in all situations where a colour value is required.
///
/// All colour channels have a f32 value that ranges from 0 to 1. However, there are facilities for
/// using a 0 to 255 range if needed.
#[derive (Copy, Clone)]
pub struct Colour {

    // Public
    /// The red component
    pub r : f32,
    /// The green component
    pub g : f32,
    /// The blue component
    pub b : f32,
    /// The alpha component
    pub a : f32
}

/*============================================================================================================*/
/*------PUBLIC FUNCTIONS--------------------------------------------------------------------------------------*/
/*============================================================================================================*/

impl Colour {

    /// Formats the colour as a string.
    ///
    /// # Examples
    /// ```
    /// let col = Colour {r : 1.0, g : 0.5, b : 0.0, a : 1.0};
    /// println! ("Colour = {}", col.to_string ());
    /// ```
    /// ```c
    /// Output : Colour = 1.0, 0.5, 0.0, 1.0
    pub fn to_string (&self) -> String {

        format! ("{}, {}, {}, {}", self.r, self.g, self.b, self.a)
    }

/*============================================================================================================*/
/*------PUBLIC STATIC FUNCTIONS-------------------------------------------------------------------------------*/
/*============================================================================================================*/

    /// Creates a colour with default values.
    ///
    /// # Examples
    /// ```
    /// let col = Colour::new (); // Resulting colour is white
    pub fn new () -> Colour {

        Colour {r : 1.0,
                g : 1.0,
                b : 1.0,
                a : 1.0}
    }

/*============================================================================================================*/

    /// Creates a colour from RGBA (0-255) value.
    ///
    /// # Examples
    /// ```
    /// let col = Colour::from_rgba (255, 0, 255, 255); // Magenta in RGBA
    /// println! ("Colour = {}", col.to_string ());
    /// ```
    /// ```c
    /// Output : Colour = 1.0, 0.0, 1.0, 1.0
    pub fn from_rgba (r : u8, g : u8, b : u8, a : u8) -> Colour {

        Colour {r : r as f32 / 255.0,
                g : g as f32 / 255.0,
                b : b as f32 / 255.0,
                a : a as f32 / 255.0}
    }

/*============================================================================================================*/

    /// Returns the colour red.
    pub fn red () -> Colour {

        Colour {r : 1.0,
                g : 0.0,
                b : 0.0,
                a : 1.0}
    }

/*============================================================================================================*/

    /// Returns the colour yellow
    pub fn yellow () -> Colour {

        Colour {r : 1.0,
                g : 1.0,
                b : 0.0,
                a : 1.0}
    }

/*============================================================================================================*/

    /// Returns the colour green.
    pub fn green () -> Colour {

        Colour {r : 0.0,
                g : 1.0,
                b : 0.0,
                a : 1.0}
    }

/*============================================================================================================*/

    /// Returns the colour cyan
    pub fn cyan () -> Colour {

        Colour {r : 0.0,
                g : 1.0,
                b : 1.0,
                a : 1.0}
    }

/*============================================================================================================*/

    /// Returns the colour blue.
    pub fn blue () -> Colour {

        Colour {r : 0.0,
                g : 0.0,
                b : 1.0,
                a : 1.0}
    }

/*============================================================================================================*/

    /// Returns the colour Magenta
    pub fn magenta () -> Colour {

        Colour {r : 1.0,
                g : 0.0,
                b : 1.0,
                a : 1.0}
    }

/*============================================================================================================*/

    /// Returns the colour black.
    pub fn black () -> Colour {

        Colour {r : 0.0,
                g : 0.0,
                b : 0.0,
                a : 1.0}
    }

/*============================================================================================================*/

    /// Returns the colour dark grey
    pub fn dark_grey () -> Colour {

        Colour {r : 0.25,
                g : 0.25,
                b : 0.25,
                a : 1.0}
    }

/*============================================================================================================*/

    /// Returns the colour grey
    pub fn grey () -> Colour {

        Colour {r : 0.5,
                g : 0.5,
                b : 0.5,
                a : 1.0}
    }

/*============================================================================================================*/

    /// Returns the colour light grey
    pub fn light_grey () -> Colour {

        Colour {r : 0.75,
                g : 0.75,
                b : 0.75,
                a : 1.0}
    }

/*============================================================================================================*/

    /// Returns the colour white.
    pub fn white () -> Colour {

        Colour {r : 1.0,
                g : 1.0,
                b : 1.0,
                a : 1.0}
    }

/*============================================================================================================*/

    /// Linearly interpolates between two colours.
    ///
    /// # Examples
    /// ```
    /// // TODO: Fill in at a later point
    pub fn lerp (start : &Colour, end : &Colour, percentage : f32) -> Colour {

        Colour {r : Mathf::lerp (start.r, end.r, percentage),
                g : Mathf::lerp (start.g, end.g, percentage),
                b : Mathf::lerp (start.b, end.b, percentage),
                a : Mathf::lerp (start.a, end.a, percentage)}
    }

/*============================================================================================================*/

    /// Linearly interpolates between two colours without clamping.
    ///
    /// # Examples
    /// ```
    /// // TODO: Fill in at a later point
    pub fn lerp_unclamped (start : &Colour, end : &Colour, percentage : f32) -> Colour {

        Colour {r : Mathf::lerp_unclamped (start.r, end.r, percentage),
                g : Mathf::lerp_unclamped (start.g, end.g, percentage),
                b : Mathf::lerp_unclamped (start.b, end.b, percentage),
                a : Mathf::lerp_unclamped (start.a, end.a, percentage)}
    }
}

/*============================================================================================================*/
/*------OPERATOR OVERLOADS------------------------------------------------------------------------------------*/
/*============================================================================================================*/

impl Add for Colour {

    type Output = Colour;

    // Addition operator (colour)
    fn add (self, rhs : Colour) -> Colour {

        Colour {r : self.r + rhs.r,
                g : self.g + rhs.g,
                b : self.b + rhs.b,
                a : self.a + rhs.a}
    }
}

/*============================================================================================================*/

impl Add <f32> for Colour {

    type Output = Colour;

    // Addition operator (f32)
    fn add (self, rhs : f32) -> Colour {

        Colour {r : self.r + rhs,
                g : self.g + rhs,
                b : self.b + rhs,
                a : self.a + rhs}
    }
}

/*============================================================================================================*/

impl AddAssign for Colour {

    // Addition assignment operator (colour)
    fn add_assign (&mut self, rhs : Colour) {

        self.r += rhs.r;
        self.g += rhs.g;
        self.b += rhs.b;
        self.a += rhs.a;
    }
}

/*============================================================================================================*/

impl AddAssign <f32> for Colour {

    // Addition assignment operator (f32)
    fn add_assign (&mut self, rhs : f32) {

        self.r += rhs;
        self.g += rhs;
        self.b += rhs;
        self.a += rhs;
    }
}

/*============================================================================================================*/

impl Sub for Colour {

    type Output = Colour;

    // Subtraction operator (colour)
    fn sub (self, rhs : Colour) -> Colour {

        Colour {r : self.r - rhs.r,
                g : self.g - rhs.g,
                b : self.b - rhs.b,
                a : self.a - rhs.a}
    }
}

/*============================================================================================================*/

impl Sub <f32> for Colour {

    type Output = Colour;

    // Subtraction operator (f32)
    fn sub (self, rhs : f32) -> Colour {

        Colour {r : self.r - rhs,
                g : self.g - rhs,
                b : self.b - rhs,
                a : self.a - rhs}
    }
}

/*============================================================================================================*/

impl Neg for Colour {

    type Output = Colour;

    // Unary minus operator
    fn neg (self) -> Colour {

        Colour {r : -self.r,
                g : -self.g,
                b : -self.b,
                a : -self.a}
    }
}

/*============================================================================================================*/

impl SubAssign for Colour {

    // Subtraction assignment operator (vector)
    fn sub_assign (&mut self, rhs : Colour) {

        self.r -= rhs.r;
        self.g -= rhs.g;
        self.b -= rhs.b;
        self.a -= rhs.a;
    }
}

/*============================================================================================================*/

impl SubAssign <f32> for Colour {

    // Subtraction assignment operator (f32)
    fn sub_assign (&mut self, rhs : f32) {

        self.r -= rhs;
        self.g -= rhs;
        self.b -= rhs;
        self.a -= rhs;
    }
}

/*============================================================================================================*/

impl Mul for Colour {

    type Output = Colour;

    // Multiplication operator (colour)
    fn mul (self, rhs : Colour) -> Colour {

        Colour {r : self.r * rhs.r,
               g : self.g * rhs.g,
               b : self.b * rhs.b,
               a : self.a * rhs.a}
    }
}

/*============================================================================================================*/

impl Mul <f32> for Colour {

    type Output = Colour;

    // Multiplication operator (f32)
    fn mul (self, rhs : f32) -> Colour {

        Colour {r : self.r * rhs,
                g : self.g * rhs,
                b : self.b * rhs,
                a : self.a * rhs}
    }
}

/*============================================================================================================*/

impl MulAssign for Colour {

    // Multiplication assignment operator (colour)
    fn mul_assign (&mut self, rhs : Colour) {

        self.r *= rhs.r;
        self.g *= rhs.g;
        self.b *= rhs.b;
        self.a *= rhs.a;
    }
}

/*============================================================================================================*/

impl MulAssign <f32> for Colour {

    // Multiplication assignment operator (f32)
    fn mul_assign (&mut self, rhs : f32) {

        self.r *= rhs;
        self.g *= rhs;
        self.b *= rhs;
        self.a *= rhs;
    }
}

/*============================================================================================================*/

impl Div for Colour {

    type Output = Colour;

    // Division operator (colour)
    fn div (self, rhs : Colour) -> Colour {

        Colour {r : self.r / rhs.r,
                g : self.g / rhs.g,
                b : self.b / rhs.b,
                a : self.a / rhs.a}
    }
}

/*============================================================================================================*/

impl Div <f32> for Colour {

    type Output = Colour;

    // Division operator (f32)
    fn div (self, rhs : f32) -> Colour {

        Colour {r : self.r / rhs,
                g : self.g / rhs,
                b : self.b / rhs,
                a : self.a / rhs}
    }
}

/*============================================================================================================*/

impl DivAssign for Colour {

    // Division assignment operator (colour)
    fn div_assign (&mut self, rhs : Colour) {

        self.r /= rhs.r;
        self.g /= rhs.g;
        self.b /= rhs.b;
        self.a /= rhs.a;
    }
}

/*============================================================================================================*/

impl DivAssign <f32> for Colour {

    // Division assignment operator (f32)
    fn div_assign (&mut self, rhs : f32) {

        self.r /= rhs;
        self.g /= rhs;
        self.b /= rhs;
        self.a /= rhs;
    }
}

/*============================================================================================================*/

impl PartialEq for Colour {

    // Equal to operator
    fn eq (&self, rhs : &Colour) -> bool {

        self.r == rhs.r &&
        self.g == rhs.g &&
        self.b == rhs.b &&
        self.a == rhs.a
    }

/*============================================================================================================*/

    // Not equal to operator
    fn ne (&self, rhs : &Colour) -> bool {

        self.r != rhs.r ||
        self.g != rhs.g ||
        self.b != rhs.b ||
        self.a != rhs.a
    }
}

/*============================================================================================================*/

// Index operator (immutable)
impl Index <u8> for Colour {

    type Output = f32;

    fn index (&self, index : u8) -> &f32 {

        match index {

            0 => return &self.r,
            1 => return &self.g,
            2 => return &self.b,
            3 => return &self.a,
            _ => unreachable! ("Index out of range for Colour")
        }
    }
}

/*============================================================================================================*/

// Index operator (mutable)
impl IndexMut <u8> for Colour {

    fn index_mut (&mut self, index : u8) -> &mut f32 {

        match index {

            0 => return &mut self.r,
            1 => return &mut self.g,
            2 => return &mut self.b,
            3 => return &mut self.a,
            _ => unreachable! ("Index out of range for Colour")
        }
    }
}
