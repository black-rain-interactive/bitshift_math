/*============================================================================================================*/
// Copyright (c) 2015, Black Rain Interactive
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice, this
//   list of conditions and the following disclaimer.
//
// * Redistributions in binary form must reproduce the above copyright notice,
//   this list of conditions and the following disclaimer in the documentation
//   and/or other materials provided with the distribution.
//
// * Neither the name of the copyright holder nor the names of its
//   contributors may be used to endorse or promote products derived from
//   this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
/*============================================================================================================*/

use Mathf;

use std::ops::*;
use std::cmp::PartialEq;

/*============================================================================================================*/
/*------STRUCTS-----------------------------------------------------------------------------------------------*/
/*============================================================================================================*/

/// 3D vector representation.
///
/// This struct represents 3D vectors and points.
/// It is used for things such as normals, mesh vertices, and 3D transformations.
#[derive (Copy, Clone)]
pub struct Vec3f {

    // Public
    /// X-axis coordinate.
    pub x : f32,
    /// Y-axis coordinate.
    pub y : f32,
    /// Z-axis coordinate.
    pub z : f32
}

/*============================================================================================================*/
/*------PUBLIC FUNCTIONS--------------------------------------------------------------------------------------*/
/*============================================================================================================*/

impl Vec3f {

    /// Formats the vector as a string.
    ///
    /// # Examples
    /// ```
    /// let vec = Vec3f {x : 10.0, y : 5.0, z : 0.0};
    /// println! ("Vector = {}", vec.to_string ());
    /// ```
    /// ```c
    /// Output : Vector = 10.0, 5.0, 0.0
    pub fn to_string (&self) -> String {

        format! ("{}, {}, {}", self.x, self.y, self.z)
    }

/*============================================================================================================*/
/*------PUBLIC STATIC FUNCTIONS-------------------------------------------------------------------------------*/
/*============================================================================================================*/

    /// Creates a vector with a default value of zero.
    ///
    /// # Examples
    /// ```
    /// let vec = Vec3f::new ();
    pub fn new () -> Vec3f {

        Vec3f {x : 0.0,
               y : 0.0,
               z : 0.0}
    }

/*============================================================================================================*/

    /// Returns the cross product of two vectors.
    pub fn cross (lhs : &Vec3f, rhs : &Vec3f) -> Vec3f {

        Vec3f {x : lhs.y * rhs.z - rhs.y * lhs.z,
               y : lhs.z * rhs.x - rhs.z * lhs.x,
               z : lhs.x * rhs.y - rhs.x * lhs.y}
    }

/*============================================================================================================*/

    /// Returns the dot product of two vectors.
    pub fn dot (lhs : &Vec3f, rhs : &Vec3f) -> f32 {

        let x_axis = lhs.x * rhs.x;
        let y_axis = lhs.y * rhs.y;
        let z_axis = lhs.z * rhs.z;

        x_axis + y_axis + z_axis
    }

/*============================================================================================================*/

    /// Returns the distance between two vectors.
    pub fn distance (start : &Vec3f, end : &Vec3f) -> f32 {

        Vec3f::length (& (*start - *end))
    }

/*============================================================================================================*/

    /// Returns the length of a vector.
    pub fn length (vector : &Vec3f) -> f32 {

        (vector.x * vector.x +
         vector.y * vector.y +
         vector.z * vector.z).sqrt ()
    }

/*============================================================================================================*/

    /// Linearly interpolates between two vectors.
    ///
    /// # Examples
    /// ```
    /// // TODO: Fill in at a later point
    pub fn lerp (start : &Vec3f, end : &Vec3f, percentage : f32) -> Vec3f {

        Vec3f {x : Mathf::lerp (start.x, end.x, percentage),
               y : Mathf::lerp (start.y, end.y, percentage),
               z : Mathf::lerp (start.z, end.z, percentage)}
    }

/*============================================================================================================*/

    /// Linearly interpolates between two vectors without clamping.
    ///
    /// # Examples
    /// ```
    /// // TODO: Fill in at a later point
    pub fn lerp_unclamped (start : &Vec3f, end : &Vec3f, percentage : f32) -> Vec3f {

        Vec3f {x : Mathf::lerp_unclamped (start.x, end.x, percentage),
               y : Mathf::lerp_unclamped (start.y, end.y, percentage),
               z : Mathf::lerp_unclamped (start.z, end.z, percentage)}
    }

/*============================================================================================================*/

    /// Get a normalized vector.
    pub fn normalize (vector : &Vec3f) -> Vec3f {

        let length = Vec3f::length (vector);

        if length != 0.0 {

            return Vec3f {x : vector.x / length,
                          y : vector.y / length,
                          z : vector.z / length};
        }

        Vec3f::new ()
    }
}

/*============================================================================================================*/
/*------OPERATOR OVERLOADS------------------------------------------------------------------------------------*/
/*============================================================================================================*/

impl Add for Vec3f {

    type Output = Vec3f;

    // Addition operator (vector)
    fn add (self, rhs : Vec3f) -> Vec3f {

        Vec3f {x : self.x + rhs.x,
               y : self.y + rhs.y,
               z : self.z + rhs.z}
    }
}

/*============================================================================================================*/

impl Add <f32> for Vec3f {

    type Output = Vec3f;

    // Addition operator (f32)
    fn add (self, rhs : f32) -> Vec3f {

        Vec3f {x : self.x + rhs,
               y : self.y + rhs,
               z : self.z + rhs}
    }
}

/*============================================================================================================*/

impl AddAssign for Vec3f {

    // Addition assignment operator (vector)
    fn add_assign (&mut self, rhs : Vec3f) {

        self.x += rhs.x;
        self.y += rhs.y;
        self.z += rhs.z;
    }
}

/*============================================================================================================*/

impl AddAssign <f32> for Vec3f {

    // Addition assignment operator (f32)
    fn add_assign (&mut self, rhs : f32) {

        self.x += rhs;
        self.y += rhs;
        self.z += rhs;
    }
}

/*============================================================================================================*/

impl Sub for Vec3f {

    type Output = Vec3f;

    // Subtraction operator (vector)
    fn sub (self, rhs : Vec3f) -> Vec3f {

        Vec3f {x : self.x - rhs.x,
               y : self.y - rhs.y,
               z : self.z - rhs.z}
    }
}

/*============================================================================================================*/

impl Sub <f32> for Vec3f {

    type Output = Vec3f;

    // Subtraction operator (f32)
    fn sub (self, rhs : f32) -> Vec3f {

        Vec3f {x : self.x - rhs,
               y : self.y - rhs,
               z : self.z - rhs}
    }
}

/*============================================================================================================*/

impl Neg for Vec3f {

    type Output = Vec3f;

    // Unary minus operator
    fn neg (self) -> Vec3f {

        Vec3f {x : -self.x,
               y : -self.y,
               z : -self.z}
    }
}

/*============================================================================================================*/

impl SubAssign for Vec3f {

    // Subtraction assignment operator (vector)
    fn sub_assign (&mut self, rhs : Vec3f) {

        self.x -= rhs.x;
        self.y -= rhs.y;
        self.z -= rhs.z;
    }
}

/*============================================================================================================*/

impl SubAssign <f32> for Vec3f {

    // Subtraction assignment operator (f32)
    fn sub_assign (&mut self, rhs : f32) {

        self.x -= rhs;
        self.y -= rhs;
        self.z -= rhs;
    }
}

/*============================================================================================================*/

impl Mul for Vec3f {

    type Output = Vec3f;

    // Multiplication operator (vector)
    fn mul (self, rhs : Vec3f) -> Vec3f {

        Vec3f {x : self.x * rhs.x,
               y : self.y * rhs.y,
               z : self.z * rhs.z}
    }
}

/*============================================================================================================*/

impl Mul <f32> for Vec3f {

    type Output = Vec3f;

    // Multiplication operator (f32)
    fn mul (self, rhs : f32) -> Vec3f {

        Vec3f {x : self.x * rhs,
               y : self.y * rhs,
               z : self.z * rhs}
    }
}

/*============================================================================================================*/

impl MulAssign for Vec3f {

    // Multiplication assignment operator (vector)
    fn mul_assign (&mut self, rhs : Vec3f) {

        self.x *= rhs.x;
        self.y *= rhs.y;
        self.z *= rhs.z;
    }
}

/*============================================================================================================*/

impl MulAssign <f32> for Vec3f {

    // Multiplication assignment operator (f32)
    fn mul_assign (&mut self, rhs : f32) {

        self.x *= rhs;
        self.y *= rhs;
        self.z *= rhs;
    }
}

/*============================================================================================================*/

impl Div for Vec3f {

    type Output = Vec3f;

    // Division operator (vector)
    fn div (self, rhs : Vec3f) -> Vec3f {

        Vec3f {x : self.x / rhs.x,
               y : self.y / rhs.y,
               z : self.z / rhs.z}
    }
}

/*============================================================================================================*/

impl Div <f32> for Vec3f {

    type Output = Vec3f;

    // Division operator (f32)
    fn div (self, rhs : f32) -> Vec3f {

        Vec3f {x : self.x / rhs,
               y : self.y / rhs,
               z : self.z / rhs}
    }
}

/*============================================================================================================*/

impl DivAssign for Vec3f {

    // Division assignment operator (vector)
    fn div_assign (&mut self, rhs : Vec3f) {

        self.x /= rhs.x;
        self.y /= rhs.y;
        self.z /= rhs.z;
    }
}

/*============================================================================================================*/

impl DivAssign <f32> for Vec3f {

    // Division assignment operator (f32)
    fn div_assign (&mut self, rhs : f32) {

        self.x /= rhs;
        self.y /= rhs;
        self.z /= rhs;
    }
}

/*============================================================================================================*/

impl PartialEq for Vec3f {

    // Equal to operator
    fn eq (&self, rhs : &Vec3f) -> bool {

        self.x == rhs.x &&
        self.y == rhs.y &&
        self.z == rhs.z
    }

/*============================================================================================================*/

    // Not equal to operator
    fn ne (&self, rhs : &Vec3f) -> bool {

        self.x != rhs.x ||
        self.y != rhs.y ||
        self.z != rhs.z
    }
}

/*============================================================================================================*/

// Index operator (immutable)
impl Index <u8> for Vec3f {

    type Output = f32;

    fn index (&self, index : u8) -> &f32 {

        match index {

            0 => return &self.x,
            1 => return &self.y,
            2 => return &self.z,
            _ => unreachable! ("Index out of range for Vec3f")
        }
    }
}

/*============================================================================================================*/

// Index operator (mutable)
impl IndexMut <u8> for Vec3f {

    fn index_mut (&mut self, index : u8) -> &mut f32 {

        match index {

            0 => return &mut self.x,
            1 => return &mut self.y,
            2 => return &mut self.z,
            _ => unreachable! ("Index out of range for Vec3f")
        }
    }
}
